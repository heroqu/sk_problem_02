# Задача 2

```
Написать клиент/сервер (tcp) использующие следующий
протокол обмена: [4 payload length, payload, crc32].
Формат payload: Uint32Array (порядок байт little-endian)
```

### Демо

установка

```
npm install
```

запуск

```
npm start
```

или

```
yarn start
```

Ожидаемый результат в консоли:

```
>yarn start
yarn run v1.9.4
$ node index.js
TCP server listens on: 127.0.0.1:8001
Connection: C1
      FROM: 127.0.0.1:54293
        TO: 127.0.0.1:8001
Connection: C2
      FROM: 127.0.0.1:54294
        TO: 127.0.0.1:8001
SERVER: a message received: [ 1, 18, 117 ]
C1: disconnected
Server just broadcasted something!
C2: a message received: [ 1, 2, 3, 5, 8 ]
SERVER: a message received: [ 1, 1, 117 ]
SERVER: a message received: [ 1, 18, 117 ]
TCP server socket is closed.
C2: disconnected
✨  Done in 3.30s.
```
