/**
 * Вспомогательные функции для операций над байтами, массивами
 * и для отправки и получения сообщений, в которые вложена полезная
 * нагрузка в виде UInt32 массивов.
 *
 * Формат сообщения:
 *
 *  Заголовок: первые 4 байта, содержит число (UInt32) байтов
 *    в полезной нагрузке
 *  Затем идет полезная нагрузка - исходные данные переведенные
 *    в последовательность байтов
 *  Футер: последние 4 байта, содержит число crc32 от байтов
 *    полезной нагрузки
 *
 * Каждая из упомянутых частей сообщения, включая Заголовок и Футер
 *  хранит исходные числа в порядке байтов little endian.
 */

const CRC32 = require('crc-32')
const net = require('net')
const assert = require('assert')

// число байтов заголовка, в котором должно
// передаваться чилсо - длина полезной нагрузки
const HEADER_LEN = 4

// число байтов футера, в котором должно храниться crc32
const CRC_LEN = 4

// Запускается на сокете и отправляет упакованные данные
function sendPayload(payload) {
  assert(this instanceof net.Socket, `Should be used in a socket context`)
  this.write(packPayload(payload))
}

// упаковка данных в требуемый формат (см README)
function packPayload(uInt32Arr) {
  if (!Array.isArray(uInt32Arr)) {
    uInt32Arr = []
  }

  const payload = uInt32ArrToBytesLE(uInt32Arr)
  const len = uInt32ArrToBytesLE([payload.length])
  const crc = uInt32ArrToBytesLE([CRC32.buf(payload)])

  return Buffer.from([...len, ...payload, ...crc])
}

/**
 * makeOnData - создает onData event handler который накапливает
 *    входящие данные, по мере готовности извлекает из них
 *    сообщения в заданном формате, извлекает из них полезную нагрузку,
 *    тут же выполняет проверку их целостности (crc32) и транслирует
 *    сами данные и результат проверки в эфир в форме события
 *
 * @param  {net.Socket} self - экземпляр socket, нужен для транслящии
 *                            извлеченных данных
 * @return {function}      - onData event handler, function(data) {...}
 */
function makeOnData(socket) {
  let buf = Buffer.allocUnsafe(0)
  let payloadBuf = null
  let payloadLen = null

  return function onData(data) {
    buf = Buffer.concat([buf, data]) // accumulate

    if (data.length <= HEADER_LEN) {
      return // ждем дальше, пока появятся байты с payload или crc
    }

    if (payloadLen === null) {
      payloadLen = buf.readUInt32LE(0) // ожидаемая длина
    }

    while (buf.length >= HEADER_LEN + payloadLen + CRC_LEN) {
      // следующее сообщение готово к извлечению

      // извлекаем payload
      payloadBuf = buf.slice(HEADER_LEN, HEADER_LEN + payloadLen)

      // проверяем CRC
      const crcIsOK =
        CRC32.buf(payloadBuf) === buf.readInt32LE(HEADER_LEN + payloadLen)

      // транслируем все в эфир для тех кому это интересно получить
      socket.emit('message', { socket, payloadBuf, crcIsOK })

      // готовимся к принятию следующего сообщения
      buf = buf.slice(HEADER_LEN + payloadLen + CRC_LEN)

      if (buf.length <= HEADER_LEN) {
        payloadLen = null
        break
      } else {
        payloadLen = buf.readUInt32LE(0)
      }
    }
  }
}

/**
 * makeOnMessage - создает onMessage event handler
 *          он извлекает полезную нагрузку из буфера
 *          и выводит сообщение в консоль
 *
 * @param  {net.Socket} socket
 * @param  {string} caption - префикс перед сообщением в консоли
 *                (например, можно указать кто это, клиент или сервер)
 * @return {function}       - onMessage event handler
 */
function makeOnMessage(socket, caption) {
  return ({ socket, payloadBuf, crcIsOK }) => {
    if (crcIsOK) {
      const payload = bufToPayload(payloadBuf)
      console.log(`${caption}: a message received:`, payload)
    } else {
      console.error(`${caption}: a corrupted message received from:
        ${socket.remoteAddress}:${socket.remotePort}`)
    }
  }
}

/**
 * Прочитывает буфер как UInt32 массив, считая что байты хранятся
 *    в порядке little-endian
 */
function bufToPayload(buf) {
  const arr = []

  if (Buffer.isBuffer(buf)) {
    let offset = 0
    const offset_max = buf.length - 4
    while (offset <= offset_max) {
      arr.push(buf.readUInt32LE(offset))
      offset += 4
    }
  }

  return arr
}


/**
 * genInt32ArrToBytesLE - генератор байтов на основе
 *  массива int32 (UInt32/Int32). Для каждого значения
 *  Integer генерирует 4 отдельных байтовых значения.
 *  Порядок байтов little-endian.
 *
 * @param  {[int32]} int32Arr - массив целых чисел (UInt32/Int32)
 * @return {*function}        - генератор
 */
function* genInt32ArrToBytesLE(int32Arr) {
  for (let i of int32Arr) {
    yield i & 0xff
    yield (i >> 8) & 0xff
    yield (i >> 16) & 0xff
    yield (i >> 24) & 0xff
  }
}

/**
 * uInt32ArrToBytesLE - переводит Int32 массив в массив Int8
 *  то есть в последовательность байтовых значений,
 *  порядок следования - little-endian.
 *
 * @param  {[int32]} int32Arr - массив целых чисел (UInt32/Int32)
 * @return {[Int8]}           - массив целых чисел (в 4 раза длиннее)
 */
function uInt32ArrToBytesLE(int32Arr) {
  return Array.from(genInt32ArrToBytesLE(int32Arr))
}

module.exports = { makeOnData, makeOnMessage, sendPayload, bufToPayload }
