const net = require('net')
const {
  makeOnData,
  makeOnMessage,
  sendPayload,
  bufToPayload
} = require('./utils')

function createClient(connName, port = 8000, host = 'localhost') {
  const option = { host, port }

  const client = net.createConnection(option, function() {
    console.log(`Connection: ${connName}
      FROM: ${client.localAddress}:${client.localPort}
        TO: ${client.remoteAddress}:${client.remotePort}`)
  })

  client.on('data', makeOnData(client))

  client.on('message', makeOnMessage(client, connName))

  client.on('end', function() {
    console.log(`${connName}: disconnected`)
  })

  client.on('error', function(err) {
    console.error(JSON.stringify(err))
  })

  client.sendPayload = sendPayload

  return client
}

module.exports = { createClient }
