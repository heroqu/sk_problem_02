const net = require('net')
const {
  makeOnData,
  makeOnMessage,
  sendPayload,
  bufToPayload
} = require('./utils')

function createServer() {
  const clients = []

  const server = net.createServer(socket => {
    clients.push(socket)

    socket.on('close', function() {
      clients.splice(clients.indexOf(socket), 1)
      if (clients.length === 0) {
        server.close(() => {
          server.unref()
        })
      }
    })

    socket.on('data', makeOnData(socket))

    socket.on('message', makeOnMessage(socket, 'SERVER'))
  })

  server.broadcastPayload = payload => {
    for (let client of clients) {
      sendPayload.call(client, payload)
    }
  }

  return server
}

module.exports = { createServer }
