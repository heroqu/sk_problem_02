const { createServer } = require('./server')
const { createClient } = require('./client')

const PORT = 8001
const HOST = 'localhost'

const SAMPLE_1 = [1, 18, 117]
const SAMPLE_2 = [1, 1, 117]
const SAMPLE_3 = [1, 2, 3, 5, 8]

function delay(ms) {
  return new Promise(resolve => setTimeout(() => resolve(), ms))
}

function startServer() {
  const server = createServer()

  server.listen(PORT, HOST, function() {
    const srvInfo = server.address()
    console.log(`TCP server listens on: ${srvInfo.address}:${srvInfo.port}`)

    server.on('close', function() {
      console.log('TCP server socket is closed.')
    })

    server.on('error', function(error) {
      console.error(JSON.stringify(error))
    })
  })

  setTimeout(() => {
    server.broadcastPayload(SAMPLE_3)
    console.log('Server just broadcasted something!')
  }, 1000)
}

async function startClient() {
  const client_1 = createClient('C1', PORT, HOST)
  const client_2 = createClient('C2', PORT, HOST)

  client_1.sendPayload(SAMPLE_1)
  client_1.end()

  await delay(1000)

  client_2.sendPayload(SAMPLE_2)

  await delay(1000)

  client_2.sendPayload(SAMPLE_1)

  await delay(1000)

  client_2.end()
}

async function start() {
  startServer()
  await delay(100)
  startClient()
}

start().catch(console.error)
